import state from './moduleTableState.js'
import mutations from './moduleTableMutations.js'
import actions from './moduleTableActions.js'
import getters from './moduleTableGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

