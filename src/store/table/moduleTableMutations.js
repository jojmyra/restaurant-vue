
export default {
  ADD_TABLE(state, item) {
    state.tables.docs.unshift(item)
    state.tables.totalDocs++
  },
  SET_TABLE(state, item) {
    state.tables = item
  },
  // SET_LABELS(state, labels) {
  //   state.eventLabels = labels
  // },
  UPDATE_TABLE(state, item) {
      const itemIndex = state.tables.docs.findIndex((p) => p._id == item._id)
      Object.assign(state.tables.docs[itemIndex], item)
  },
  REMOVE_TABLE(state, itemId) {
      const ItemIndex = state.tables.docs.findIndex((p) => p._id == itemId)
      state.tables.docs.splice(ItemIndex, 1)
      state.tables.totalDocs--
  },
}
