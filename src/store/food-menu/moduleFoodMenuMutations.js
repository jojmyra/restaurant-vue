export default {
  ADD_ITEM(state, item) {
    state.menus.docs.unshift(item)
    state.menus.totalDocs++
  },
  SET_MENUS(state, menu) {
    state.menus = menu
  },
  // SET_LABELS(state, labels) {
  //   state.eventLabels = labels
  // },
  UPDATE_ITEM(state, menu) {
      const menuIndex = state.menus.docs.findIndex((p) => p._id == menu._id)
      Object.assign(state.menus.docs[menuIndex], menu)
  },
  REMOVE_ITEM(state, itemId) {
      const ItemIndex = state.menus.docs.findIndex((p) => p._id == itemId)
      state.menus.docs.splice(ItemIndex, 1)
      state.menus.totalDocs--
  },
}
