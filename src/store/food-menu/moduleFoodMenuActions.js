import axios from "@/axios.js";

export default {
  addItem({ commit }, item) {
    const formData = new FormData();
    if (item.foodName) formData.append("foodName", item.foodName);
    if (item.foodType) formData.append("foodType", item.foodType);
    if (item.price) formData.append("price", item.price);
    if (item.describe) formData.append("describe", item.describe);
    if (item.calroies) formData.append("calroies", item.calroies);
    if (item.dataImg) formData.append("foodImage", item.dataImg);
    
    return new Promise((resolve, reject) => {
      axios
        .post("menu", formData)
        .then(response => {
          if (response.data.success) {
            commit(
              "ADD_ITEM",
              Object.assign(item, { _id: response.data.data._id })
            );
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  fetchMenus({ commit }, options) {
    var parms = JSON.stringify(options);
    return new Promise((resolve, reject) => {
      axios
        .get(`menu?options=${parms}`)
        .then(response => {
          commit("SET_MENUS", response.data);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  updateItem({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .put(`menu/${item._id}`, { item: item })
        .then(response => {
          // mongo return old value ถ้าใส่สำเร็จเปลี่ยนเป็นตัวที่แก้ไข
          if (response.data.success) {
            commit("UPDATE_ITEM", item);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  removeItem({ commit }, itemId) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`menu/${itemId}`)
        .then(response => {
          if (response.data.success) {
            commit("REMOVE_ITEM", itemId);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
