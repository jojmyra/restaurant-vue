import state from './moduleFoodMenuState.js'
import mutations from './moduleFoodMenuMutations.js'
import actions from './moduleFoodMenuActions.js'
import getters from './moduleFoodMenuGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

