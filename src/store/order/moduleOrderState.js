export default {
  orderId: '',
  orders: {
    menu: [],
    tableId: null,
    customerNumber: null,
    user: null
  },
  kitchens: {
    menu: [],
    user: null
  },
  history: {
    menu: []
  },
  payment: {
    billId: null
  },
  print: []
}
