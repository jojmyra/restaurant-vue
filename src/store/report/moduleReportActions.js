import axios from "@/axios.js";

export default {
  getReport({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_REPORT", response.data.data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getReportRevenue({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report/month`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getReportRevenueYear({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report/year`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getReportSummary({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report/summary`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getReportSummaryYear({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report/summary/year`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getReportCard({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report/card`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_REPORT", response.data.data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getReportBill({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report/bill`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_REPORT_BILL", response.data.data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getReportCancel({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/report/cancel`, {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_REPORT_CANCEL", response.data.data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
