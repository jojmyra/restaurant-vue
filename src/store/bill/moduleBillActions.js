import axios from "@/axios.js";

export default {
  addOrder({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post("order", { item: item })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_ORDER_BLANK")
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  acceptOrder({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post("order/accept", { item: item })
        .then(response => {
          if (response.data.success) {
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getOrder({ commit }, item) {
    commit("SET_ORDER_ID", item)
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/${item}`)
        .then(response => {          
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_ORDER_BY_STATUS", response.data.data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getOrderKitchen({ commit }, options) {
    var parms = JSON.stringify(options);
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/wait/?options=${parms}`)
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_ORDER_KITCHEN", response.data.data)
            
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getOrderHistory({ commit }, userId) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/order/history/${userId}`)
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit('SET_ORDER_HISTORY', response.data.data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  serve({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .put(`/order/serve`, { item: item })
        .then(response => {
          if (response.data.success) {
            // แก้ไขข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  cancelOrder({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .put(`/order`, { item: item })
        .then(response => {
          if (response.data.success) {
            // แก้ไขข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  cancelSuccess({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .put(`/order/cancelSuccess`, { item: item })
        .then(response => {
          if (response.data.success) {
            // แก้ไขข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  changeTable({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post(`/order/changeTable`, { item: item })
        .then(response => {
          if(response.data.success) {

          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
