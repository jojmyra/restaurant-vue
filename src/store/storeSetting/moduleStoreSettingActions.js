import axios from "@/axios.js";

export default {
  settingStore({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post("setting", { item: item })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("CONFIG_STORE", response.data.data);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  fetchStoreSetting({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get(`setting`)
        .then(response => {
          commit("CONFIG_STORE", response.data);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
