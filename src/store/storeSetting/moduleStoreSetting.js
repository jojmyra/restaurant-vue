import state from './moduleStoreSettingState.js'
import mutations from './moduleStoreSettingMutations.js'
import actions from './moduleStoreSettingActions.js'
import getters from './moduleStoreSettingGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

