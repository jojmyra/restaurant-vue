export default {
  SET_CASHIER(state, item) {
    state.data.cashier = item.cashier
    state.data.initialMoney = item.initialMoney
    state.data.orderValue = item.orderValue
  },
  REMOVE_CASHIER(state) {
    state.data.cashier = ''
    state.data.initialMoney = null
    state.data.orderValue = null
  },
  SET_CASHIER_LOG(state, item) {
    state.log = item
  }
};
