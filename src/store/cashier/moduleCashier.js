import state from './moduleCashierState.js'
import mutations from './moduleCashierMutations.js'
import actions from './moduleCashierActions.js'
import getters from './moduleCashierGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

