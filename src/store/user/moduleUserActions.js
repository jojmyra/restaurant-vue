import axios from "@/axios.js";

export default {
  addItem({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post("user", { item: item })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit(
              "ADD_USER",
              Object.assign(item, { _id: response.data.data._id })
            );
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  fetchUsers({ commit }, options) {
    var parms = JSON.stringify(options);
    return new Promise((resolve, reject) => {
      axios
        .get(`user?options=${parms}`)
        .then(response => {
          commit("SET_USERS", response.data);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  fetchCashiers({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get(`user/cashier`, {
          params: item
        })
        .then(response => {
          resolve(response.data.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  updateItem({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .put(`user/${item._id}`, { item: item })
        .then(response => {
          if (response.data.success) {
            commit("UPDATE_USER", item);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  removeItem({ commit }, itemId) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`user/${itemId}`)
        .then(response => {
          if (response.data.success) {
            commit("REMOVE_USER", itemId);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  changePassword({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post(`user/changepassword`, { item: item })
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
