export default {
  ADD_USER(state, item) {
    state.users.docs.unshift(item)
    state.users.totalDocs++
  },
  SET_USERS(state, user) {
    state.users = user
  },
  // SET_LABELS(state, labels) {
  //   state.eventLabels = labels
  // },
  UPDATE_USER(state, user) {
      const userIndex = state.users.docs.findIndex((p) => p._id == user._id)
      Object.assign(state.users.docs[userIndex], user)
  },
  REMOVE_USER(state, itemId) {
      const ItemIndex = state.users.docs.findIndex((p) => p._id == itemId)
      state.users.docs.splice(ItemIndex, 1)
      state.users.totalDocs--
  },
}
